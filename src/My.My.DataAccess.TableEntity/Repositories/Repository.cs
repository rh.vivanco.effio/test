
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure.Storage;
using My.My.DataAccess.Core.Interfaces;
using My.My.Util.Options;
using Microsoft.Extensions.Options;

namespace My.My.DataAccess.TableEntity.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, ITableEntity, new()
    {
        private readonly CloudTable _table;
        readonly string _tableName;
        public Repository(IOptions<StorageAccountOptions> options, string tableTargetName)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(options.Value.StorageConnectionString);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

            var prefix = $"";
            this._tableName = $"{prefix}{tableTargetName}";

            this._table = tableClient.GetTableReference(this._tableName);
            var created = this._table.CreateIfNotExistsAsync().GetAwaiter().GetResult();
        }
        
        public async Task Add(TEntity document)
        {
            TableOperation insertOperation = TableOperation.Insert(document);
            await this._table.ExecuteAsync(insertOperation);
        }

        public async Task AddBatch(IEnumerable<TEntity> documents)
        {
            TableBatchOperation batch = new TableBatchOperation();

            foreach (var item in documents)
            {
                batch.Add(TableOperation.InsertOrMerge(item));
            }
            await this._table.ExecuteBatchAsync(batch);
        }

        public Task<IEnumerable<TEntity>> Find(Expression<Func<TEntity, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Task<TEntity> FindFirst(Expression<Func<TEntity, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<TEntity> GetById(string partitionKey, string id)
        {
            TableOperation queryOperation = TableOperation.Retrieve<TEntity>(partitionKey, id);
            var entity = await this._table.ExecuteAsync(queryOperation);

            if (entity.Result != null)
                return (TEntity)entity.Result;

            return default(TEntity);
        }

        public async Task<IEnumerable<TEntity>> GetAll()
        {
            TableContinuationToken token = null;
            var entities = new List<TEntity>();
            do
            {
                var queryResult = await this._table.ExecuteQuerySegmentedAsync(new TableQuery<TEntity>(), token).ConfigureAwait(false);
                entities.AddRange(queryResult.Results);
                token = queryResult.ContinuationToken;
            } while (token != null);

            return entities;
        }

        public async Task Remove(TEntity document)
        {
            TableOperation deleteOperation = TableOperation.Delete(document);
            await this._table.ExecuteAsync(deleteOperation);
        }

        public async Task Update(TEntity document)
        {
            TableOperation updateOperation = TableOperation.InsertOrReplace(document);
            await this._table.ExecuteAsync(updateOperation);
        }

        protected async Task<IEnumerable<TEntity>> Query(TableQuery<TEntity> query)
        {
            var entities = new List<TEntity>();

            TableContinuationToken token = null;
            do
            {
                var queryResult = await this._table.ExecuteQuerySegmentedAsync(query, token).ConfigureAwait(false);
                entities.AddRange(queryResult.Results);
                token = queryResult.ContinuationToken;
            } while (token != null);

            return entities;
        }

        protected async Task<IEnumerable<TEntity>> QueryPaging(TableQuery<TEntity> query)
        {
            var entities = new List<TEntity>();

            const int maxEntitiesPerQueryLimit = 1000;
            var totalTakeCount = query.TakeCount;
            var remainingRecordsToTake = query.TakeCount;

            TableContinuationToken token = null;
            do
            {
                query.TakeCount = remainingRecordsToTake >= maxEntitiesPerQueryLimit ? maxEntitiesPerQueryLimit : remainingRecordsToTake;
                remainingRecordsToTake -= query.TakeCount;

                var queryResult = await this._table.ExecuteQuerySegmentedAsync(query, token).ConfigureAwait(false);
                entities.AddRange(queryResult.Results);
                token = queryResult.ContinuationToken;
            } while (entities.Count < totalTakeCount && token != null);

            return entities;
        }

        public void Dispose()
        {
           
        }
    }
}

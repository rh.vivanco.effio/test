using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using My.My.BusinessLogic.Interfaces;
using My.My.BusinessLogic.Services;
using My.My.BusinessLogic.Services.Interfaces;
using My.My.CrossCutting.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace My.My.CrossCutting.IoC
{
    public static class ApplicationExtensions
    {
        public static void AddApplicationServices(this IServiceCollection services, IConfiguration configuration)
        {
            // Business Logic
            services.AddScoped<IAppSettingQueryService, AppSettingQueryService>();
            services.AddScoped<IAppSettingService, AppSettingService>();

            // CrossCutting
            services.AddScoped<IIdentityService, IdentityService>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }
    }
}

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using My.My.DataAccess.Core.Interfaces;
using My.My.DataAccess.TableEntity.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace My.My.CrossCutting.IoC
{
    public static class RepositoryExtensions
    {
        public static void AddRepositories(this IServiceCollection services, IConfiguration configuration)
        {
            // Data Access
            services.AddScoped<IAppSettingRepository, AppSettingRepository>();

        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace My.My.BusinessLogic.Core
{
    public enum BusinessManagerMessageType
    {
        NotFound,
        Conflict,
        Result
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace My.My.DataAccess.Core.Models
{
    public class AppSetting : EntityBase
    {
        [Required]
        public string Value { get; set; }

        public static class Factory
        {
            public static AppSetting Create(string partition, string id, string value, string createdBy)
            {
                var entity = new AppSetting()
                {
                    RowKey = id,
                    PartitionKey = partition,
                    Value = value,
                    CreatedBy = createdBy,
                    CreatedOn = DateTime.UtcNow,
                    UpdatedBy = createdBy,
                };

                return entity;
            }
        }
    }
}

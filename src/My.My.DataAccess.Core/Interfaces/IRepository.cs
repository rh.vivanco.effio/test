using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace My.My.DataAccess.Core.Interfaces
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        Task Add(TEntity document);
        Task AddBatch(IEnumerable<TEntity> documents);
        Task Update(TEntity document);
        Task Remove(TEntity document);
        Task<IEnumerable<TEntity>> Find(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> FindFirst(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> GetById(string partition, string id);
        Task<IEnumerable<TEntity>> GetAll();
    }
}

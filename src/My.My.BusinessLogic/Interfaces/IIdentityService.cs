using System;
using System.Collections.Generic;
using System.Text;

namespace My.My.BusinessLogic.Interfaces
{
    public interface IIdentityService
    {
        string GetUserId();
    }
}

using System;
using System.Threading.Tasks;
using My.My.BusinessLogic.Constants;
using My.My.BusinessLogic.Interfaces;
using My.My.BusinessLogic.Models;
using My.My.BusinessLogic.Services.Interfaces;
using My.My.DataAccess.Core.Interfaces;
using My.My.DataAccess.Core.Models;

namespace My.My.BusinessLogic.Services
{
    public class AppSettingService : IAppSettingService
    {
        private readonly IAppSettingRepository _appSettingDataService;
        private readonly IIdentityService _identityService;

        public AppSettingService(IAppSettingRepository appSettingRepository,
                                 IIdentityService identityService)
        {
            this._appSettingDataService = appSettingRepository;
            this._identityService = identityService;
        }

        public async Task<BusinessResultRp> CreateAppSetting(AppSettingPostRp resource)
        {
            var result = new BusinessResultRp();

            var createdBy = this._identityService.GetUserId();
            var appSetting = AppSetting.Factory.Create(DefaultConstantNames.DefaultPartitionKey, resource.Id, resource.Value, createdBy);

            var entity = await this._appSettingDataService.GetById(DefaultConstantNames.DefaultPartitionKey, resource.Id);
            if (entity != null)
            {
                result.AddConflict($"The Id {resource.Id} has already been taken.");
                return result;
            }

            await this._appSettingDataService.Add(appSetting);
            
            result.AddResult("Key", appSetting.RowKey);

            return result;
        }

        public async Task<BusinessResultRp> UpdateAppSetting(string id, AppSettingPutRp resource)
        {
            var result = new BusinessResultRp();

            var appSetting = await this._appSettingDataService.GetById(DefaultConstantNames.DefaultPartitionKey, id);

            if (appSetting == null)
            {
                result.AddNotFound($"The Id {id} doesn't exists.");
                return result;
            }

            appSetting.Value = resource.Value;

            appSetting.Update(this._identityService.GetUserId());

            await this._appSettingDataService.Update(appSetting);

            return result;
        }

        public async Task<BusinessResultRp> DeleteAppSetting(string id)
        {
            var result = new BusinessResultRp();

            var appSetting = await this._appSettingDataService.GetById(DefaultConstantNames.DefaultPartitionKey, id);

            if (appSetting == null)
            {
                result.AddNotFound($"The Id {id} doesn't exists.");
                return result;
            }

            appSetting.Delete();

            appSetting.Update(this._identityService.GetUserId());

            await this._appSettingDataService.Update(appSetting);

            return result;
        }

    }
}
